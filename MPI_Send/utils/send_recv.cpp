#include <mpi.h>
#include <unistd.h>

#include <iostream>

void FillBuffer(char* buf, int chunk_size) {
  for (int i = 0; i < chunk_size; ++i) {
    buf[i] = static_cast<char>(i % 10 + 48);
  }
}

void DoSend(int rank, int chunk_size, char* buf) {
  double start_time = MPI_Wtime();
  MPI_Send(buf, chunk_size, MPI_CHAR, 1, 0, MPI_COMM_WORLD);

  std::cout << "Time for send is " << MPI_Wtime() - start_time << std::endl;
}

void DoSsend(int rank, int chunk_size, char* buf) {
  double start_time = MPI_Wtime();
  MPI_Ssend(buf, chunk_size, MPI_CHAR, 1, 0, MPI_COMM_WORLD);

  std::cout << "Time for ssend is " << MPI_Wtime() - start_time << std::endl;
}

void DoBsend(int rank, int chunk_size, char* buf) {
  double start_time = MPI_Wtime();
  MPI_Bsend(buf, chunk_size, MPI_CHAR, 1, 0, MPI_COMM_WORLD);

  std::cout << "Time for bsend is " << MPI_Wtime() - start_time << std::endl;
}

void DoIsend(int rank, int chunk_size, char* buf) {
  double start_time = MPI_Wtime();
  MPI_Request request;

  MPI_Isend(buf, chunk_size, MPI_CHAR, 1, 0, MPI_COMM_WORLD, &request);

  std::cout << "Time for isend is " << MPI_Wtime() - start_time << std::endl;
}

void DoRsend(int rank, int chunk_size, char* buf) {
  double start_time = MPI_Wtime();
  MPI_Request request;

  sleep(3);  // try to comment this sleep

  MPI_Rsend(buf, chunk_size, MPI_CHAR, 1, 0, MPI_COMM_WORLD);

  std::cout << "Time for rsend is " << MPI_Wtime() - start_time << std::endl;
}

void DoRecv(int rank, int chunk_size, char* buf, MPI_Status status) {
  double start_time = MPI_Wtime();
  // std::cout << "For " << rank << " start time is " << start_time <<
  // std::endl;

  sleep(2);

  MPI_Recv(buf, chunk_size, MPI_CHAR, 0, 0, MPI_COMM_WORLD, &status);

  std::cout << "Time for receive is " << MPI_Wtime() - start_time << std::endl;
}

void DoSendTestForOneChunkSize(int rank, int commsize, int chunk_size,
                               int send_type) {
  char* buf = new char[chunk_size];
  MPI_Status status;

  MPI_Barrier(MPI_COMM_WORLD);

  if (rank == 0) {
    FillBuffer(buf, chunk_size);
  }

  MPI_Barrier(MPI_COMM_WORLD);

  if (rank == 0) {
    if (send_type == 0) {
      DoSend(rank, chunk_size, buf);
    } else if (send_type == 1) {
      DoSsend(rank, chunk_size, buf);
    } else if (send_type == 2) {
      DoBsend(rank, chunk_size, buf);
    } else if (send_type == 3) {
      DoIsend(rank, chunk_size, buf);
    } else if (send_type == 4) {
      DoRsend(rank, chunk_size, buf);
    }
  }
  if (rank == 1) {
    DoRecv(rank, chunk_size, buf, status);
  }

  std::cout.flush();

  MPI_Barrier(MPI_COMM_WORLD);

  delete[] buf;
}

void DoSendTest(int rank, int commsize, int send_type) {
  for (int chunk_size = 10; chunk_size <= 1'000'000'000; chunk_size *= 10) {
    if (rank == 0) {
      std::cout << std::endl;
      std::cout << "Chunk size is " << chunk_size << std::endl;
    }
    DoSendTestForOneChunkSize(rank, commsize, chunk_size, send_type);
  }
}
