#include "init.h"

#include <stdio.h>

int Init(int argc, char* argv[], int* size, int* n_threads) {
  if (argc != 3) {
    fprintf(stderr, "Usage: ./<app> <int array size> <int threads number>\n");
    return 1;
  }

  sscanf(argv[1], "%d", size);
  sscanf(argv[2], "%d", n_threads);

  return 0;
}
