#!/bin/bash


g++ main.cpp -fopenmp

for i in {1000..5000..100}
do
  echo Matrix size is $i
  ./a.out $i
done

