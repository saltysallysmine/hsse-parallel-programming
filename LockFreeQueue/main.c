#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "queue/queue.h"

#define COUNT 10000000

// Locks

pthread_mutex_t mutex;
// pthread_spinlock_t spinlock;

void queue_push_simple(queue_t* queue, int value) {
  node_t* node = (node_t*)malloc(sizeof(node_t));
  node->value = value;
  node->next = NULL;

  pthread_mutex_lock(&mutex);
  // pthread_spin_lock(&spinlock);
  queue->tail->next = node;
  queue->tail = node;
  pthread_mutex_unlock(&mutex);
  // pthread_spin_unlock(&spinlock);
}

int queue_pop_simple(queue_t* queue, int* value) {
  pthread_mutex_lock(&mutex);
  // pthread_spin_lock(&spinlock);
  node_t* node = queue->head;
  node_t* new_head = node->next;
  if (new_head == NULL) {
    pthread_mutex_unlock(&mutex);
    // pthread_spin_unlock(&spinlock);
    return 1;
  }
  *value = new_head->value;
  queue->head = new_head;
  pthread_mutex_unlock(&mutex);
  // pthread_spin_unlock(&spinlock);
  return 0;
}

// Lock-free queue ops

queue_t* queue;

void* push_from_threads(void* args) {
  for (int i = 0; i < COUNT; ++i) {
    queue_push(queue, i);
  }

  return NULL;
}

void* pop_from_threads(void* args) {
  for (int i = 0; i < COUNT; ++i) {
    int value;
    queue_pop(queue, &value);
  }

  return NULL;
}

// Simple ops

void* simple_push_from_threads(void* args) {
  for (int i = 0; i < COUNT; ++i) {
    queue_push_simple(queue, i);
  }

  return NULL;
}

void* simple_pop_from_threads(void* args) {
  for (int i = 0; i < COUNT; ++i) {
    int value;
    queue_pop_simple(queue, &value);
  }

  return NULL;
}

void test_lock_free_queue() {
  pthread_t thread0;
  pthread_t thread1;

  // Do pushes
  pthread_create(&thread0, NULL, push_from_threads, NULL);
  pthread_create(&thread1, NULL, push_from_threads, NULL);

  pthread_join(thread0, NULL);
  pthread_join(thread1, NULL);

  printf("Len after pushes is %d\n", queue_length(queue));

  // Do pops
  // pthread_create(&thread0, NULL, pop_from_threads, NULL);
  // pthread_create(&thread1, NULL, pop_from_threads, NULL);

  // pthread_join(thread0, NULL);
  // pthread_join(thread1, NULL);

  printf("Len after pops is %d\n", queue_length(queue));
}

void test_simple_queue() {
  pthread_mutex_init(&mutex, NULL);
  // pthread_spin_init(&spinlock, 0);

  pthread_t thread0;
  pthread_t thread1;

  // Do pushes
  pthread_create(&thread0, NULL, simple_push_from_threads, NULL);
  pthread_create(&thread1, NULL, simple_push_from_threads, NULL);

  pthread_join(thread0, NULL);
  pthread_join(thread1, NULL);

  printf("Len after simple pushes is %d\n", queue_length(queue));

  // Do pops
  // pthread_create(&thread0, NULL, simple_pop_from_threads, NULL);
  // pthread_create(&thread1, NULL, simple_pop_from_threads, NULL);

  // pthread_join(thread0, NULL);
  // pthread_join(thread1, NULL);

  printf("Len after simple pops is %d\n", queue_length(queue));
}

// Utils

double get_elapsed_time_ms() {
  struct timespec ct;
  clock_gettime(CLOCK_MONOTONIC, &ct);
  return (double)ct.tv_sec * 1000.0 + (double)ct.tv_nsec / 1000000.0;
}

int main() {
  queue_init(&queue);

  double start = get_elapsed_time_ms();
  // test_lock_free_queue();
  test_simple_queue();
  printf("Total time is: %f\n", get_elapsed_time_ms() - start);

  return 0;
}
