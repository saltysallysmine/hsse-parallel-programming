#include <mpi.h>

#include <iomanip>
#include <iostream>

int main(int argc, char* argv[]) {
  double start_time;
  double end_time;

  if (argc != 2) {
    std::cerr << "Exactly one number should be passed as an argument\n";
    return 1;
  }

  int series_length;
  std::string input(argv[1]);

  try {
    series_length = std::stoi(input);
  } catch (...) {
    std::cerr << "Error: Invalid input for conversion" << std::endl;
    return 1;
  }

  int err = MPI_Init(&argc, &argv);
  if (err != 0) {
    return 1;
  }

  int rank;
  err = MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  if (err != 0) {
    return 1;
  }

  if (rank == 0) {
    start_time = MPI_Wtime();

    long double result = 0.;
    for (int i = 1; i <= series_length; ++i) {
      result += 1. / static_cast<long double>(i);
    }
    std::cout << "Baseline result is: " << std::setprecision(9) << result
              << std::endl;

    end_time = MPI_Wtime();
    std::cout << "Working time: " << std::setprecision(9)
              << end_time - start_time << std::endl;
  }
  return MPI_Finalize();
}
