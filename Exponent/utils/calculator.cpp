#include <gmp.h>
#include <gmpxx.h>
#include <mpi.h>

#include <iomanip>
#include <iostream>

int batch_size = 10;

void SendFactorial(int receiver, mpz_t factorial) {}

void CalculateModulus(int precision, mpz_t& result) {
  mpz_t base;
  mpz_init(base);
  mpz_set_ui(base, 10);
  mpz_pow_ui(result, base, precision);
  mpz_clear(base);
}

void CalculateFactorials(int commsize, int precision) {
  mpz_t factorial;
  mpz_init(factorial);
  CalculateModulus(precision, factorial);

  mpz_t denominator;
  mpz_init(denominator);
  mpz_set_ui(denominator, 1);

  mpz_t end_state;
  mpz_init(end_state);
  mpz_set_ui(end_state, 0);

  int receiver = 1;
  int denominator_id = 1;

  while (true) {
    mpz_div(factorial, factorial, denominator);

    if (denominator_id % batch_size == 0) {
      receiver = receiver % (commsize - 1) + 1;
      SendFactorial(receiver, factorial);
    }

    ++denominator_id;
    mpz_add_ui(denominator, denominator, 1);

    if (mpz_cmp(factorial, end_state) == 0) {
      break;
    }
  }

  mpz_clear(factorial);
}

void CalculateSumPart(int start, int step, int precision, long double& answer) {
  for (int i = start; i <= precision; i += step) {
    answer += 1. / static_cast<long double>(i);
  }
  // std::cout << "calculating " << answer << " in " << start - 1 << "\n";
}

void CalculateExponent(int rank, int commsize, int precision,
                       long double& result) {
  long double local_result = 0;
  CalculateSumPart(rank + 1, commsize, precision, local_result);

  MPI_Reduce(&local_result, &result, 1, MPI_LONG_DOUBLE, MPI_SUM, 0,
             MPI_COMM_WORLD);
}

void MeasureExponentCalculation(int rank, int commsize, int precision) {
  double start_time;
  double end_time;

  if (rank == 0) {
    start_time = MPI_Wtime();
  }

  long double result = 0;
  if (rank == 0) {
    CalculateFactorials(commsize, precision);
  } else {
    CalculateExponent(rank, commsize, precision, result);
  }

  if (rank == 0) {
    std::cout << "Global result is " << std::setprecision(9) << result
              << std::endl;

    end_time = MPI_Wtime();
    std::cout << "Working time: " << std::setprecision(9)
              << end_time - start_time << std::endl;
  }
}
