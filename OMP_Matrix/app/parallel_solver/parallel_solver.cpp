#include <omp.h>

#include <iostream>
#include <vector>

#include "../model/matrix.h"

class ParallelSolver {
 public:
  void Multiply(Matrix& a, Matrix& b, Matrix& result);
};

void ParallelSolver::Multiply(Matrix& a, Matrix& b, Matrix& result) {
  int size = (int)a.size();
#pragma omp parallel for
  for (int line_ind = 0; line_ind < size; ++line_ind) {
#pragma omp parallel for
    for (int col_ind = 0; col_ind < size; ++col_ind) {
      for (int i = 0; i < size; ++i) {
        result[line_ind][col_ind] += a[line_ind][i] * b[i][col_ind];
      }
    }
  }
}
