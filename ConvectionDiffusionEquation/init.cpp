#include <mpi.h>

#include <iostream>

int ParseArguments(int argc, char* argv[], int& max_x, int& max_t) {
  if (argc != 3) {
    std::cerr << "Exactly two numbers should be passed in arguments\n";
    return 1;
  }

  std::string input_x(argv[1]);
  std::string input_t(argv[2]);

  try {
    max_x = std::stoi(input_x);
  } catch (...) {
    std::cerr << "Error: Invalid max_x input for conversion" << std::endl;
    return 1;
  }

  try {
    max_t = std::stoi(input_t);
  } catch (...) {
    std::cerr << "Error: Invalid max_t input for conversion" << std::endl;
    return 1;
  }
  return 0;
}

int Init(int argc, char* argv[], int& rank, int& commsize) {
  int err = MPI_Init(&argc, &argv);
  if (err != 0) {
    return 1;
  }

  err = MPI_Comm_size(MPI_COMM_WORLD, &commsize);
  if (err != 0) {
    return 1;
  }

  err = MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  if (err != 0) {
    return 1;
  }

  return 0;
}
