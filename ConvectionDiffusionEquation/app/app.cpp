#include <mpi.h>

#include <iomanip>
#include <iostream>
#include <vector>

#include "manager/manager.cpp"
#include "worker/worker.cpp"

void Main(int rank, int commsize, int max_x, int max_t) {
  int chunk_size = (max_x - 1) / (commsize - 1);
  int last_chunk_size = (max_x - 1) - chunk_size * (commsize - 2);

  if (rank == 0) {
    Manager manager(commsize, max_x, max_t, chunk_size, last_chunk_size);
    manager.Poll();

  } else if (rank == commsize - 1) {
    int left_x = (commsize - 2) * chunk_size;
    int right_x = left_x + last_chunk_size - 1;

    Worker worker(rank, max_x, max_t, last_chunk_size, left_x, right_x);
    worker.Poll();

  } else {
    Worker worker(rank, max_x, max_t, chunk_size);
    worker.Poll();
  }
}
