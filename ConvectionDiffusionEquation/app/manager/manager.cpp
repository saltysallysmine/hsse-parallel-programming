#include <mpi.h>

#include <iomanip>
#include <iostream>
#include <vector>

#include "functions.cpp"

class Manager {
  int commsize;
  int max_x;
  int max_t;
  int chunk_size;
  int last_chunk_size;

  std::vector<std::vector<long double>> data;
  std::vector<int> send_batches_cnt;
  std::vector<int> received_batches_cnt;

  void _InitializeData();
  void _SendNeighbourPoints();
  void _SendChunks();
  void _ReceiveChunks();
  void _PrintData();

 public:
  Manager(int commsize, int max_x, int max_t, int chunk_size,
          int last_chunk_size)
      : commsize(commsize),
        max_x(max_x),
        max_t(max_t),
        chunk_size(chunk_size),
        last_chunk_size(last_chunk_size) {
    _InitializeData();
  };

  void Poll();
};

// Definitions

void Manager::_InitializeData() {
  data.resize(max_t, std::vector<long double>(max_x + 1));

  for (int x = 0; x < max_x; ++x) {
    data[0][x] = Phi(x);
  }

  for (int t = 0; t < max_t; ++t) {
    data[t][0] = Psi(t);
  }

  send_batches_cnt.resize(commsize + 1, 0);
  received_batches_cnt.resize(commsize + 1, 0);
}

void Manager::_SendNeighbourPoints() {
  puts("Send neighbour points");

  MPI_Request request;

  for (int i = 1; i < commsize; ++i) {
    if (received_batches_cnt[i] >= max_t) {  // task #i done
      continue;
    }

    if (i == 1 || received_batches_cnt[i - 1] >= send_batches_cnt[i]) {
      int neghbour_point_ind = (i - 1) * chunk_size;

      MPI_Isend(&data[received_batches_cnt[i]][neghbour_point_ind], 1,
                MPI_LONG_DOUBLE, i, 0, MPI_COMM_WORLD, &request);

      ++send_batches_cnt[i];
    }
  }
}

void Manager::_SendChunks() {
  puts("Send chunks");
  MPI_Request request;

  for (int i = 1; i < commsize - 1; ++i) {
    long double* chunk_buffer_ptr = &data[0][(i - 1) * chunk_size + 1];

    MPI_Isend(chunk_buffer_ptr, chunk_size, MPI_LONG_DOUBLE, i, 0,
              MPI_COMM_WORLD, &request);
  }

  // send chunk to the last worker
  long double* last_chunk_buffer_ptr =
      &data[0][(commsize - 2) * chunk_size + 1];

  MPI_Isend(last_chunk_buffer_ptr, last_chunk_size, MPI_LONG_DOUBLE,
            commsize - 1, 0, MPI_COMM_WORLD, &request);
}

void Manager::_ReceiveChunks() {
  puts("Receive chunks");

  std::vector<MPI_Request> requests(commsize);

  for (int i = 1; i < commsize - 1; ++i) {
    long double* recv_addr =
        &data[received_batches_cnt[i] + 1][(i - 1) * chunk_size + 1];
    MPI_Irecv(recv_addr, chunk_size, MPI_LONG_DOUBLE, i, 0, MPI_COMM_WORLD,
              &requests[i]);
  }

  // get chunk from the last worker
  long double* recv_addr = &data[received_batches_cnt[commsize - 1] + 1]
                                [(commsize - 2) * chunk_size + 1];
  MPI_Irecv(recv_addr, last_chunk_size, MPI_LONG_DOUBLE, commsize - 1, 0,
            MPI_COMM_WORLD, &requests[commsize - 1]);

  for (int i = 1; i < commsize; ++i) {
    int recv_finished = false;
    MPI_Status status;
    while (!recv_finished) {
      MPI_Request_get_status(requests[i], &recv_finished, &status);
    }
    ++received_batches_cnt[i];
  }
}

void Manager::_PrintData() {
  for (int i = 0; i < max_x; ++i) {
    std::vector<long double>& line = data[i];

    std::cout << i << ": ";

    for (auto el : line) {
      std::cout << std::setprecision(9) << std::fixed << el << " ";
    }
    std::cout << std::endl;
  }
}

void Manager::Poll() {
  _SendChunks();

  for (int layer_number = 0; layer_number < max_t - 1; ++layer_number) {
    puts("\n");
    printf("Layer #%d", layer_number);
    puts("\n");

    _SendNeighbourPoints();
    _ReceiveChunks();
  }

  puts("\nAll tasks performed\n");

  _PrintData();
}
