#include <mpi.h>

#include <iostream>

#include "utils/init.cpp"
#include "utils/work.cpp"

int main(int argc, char* argv[]) {
  int rank, commsize;
  int err = Init(argc, argv, rank, commsize);
  if (err != 0 || commsize < 2) {
    return err;
  }

  DoMain(rank, commsize);

  return MPI_Finalize();
}
