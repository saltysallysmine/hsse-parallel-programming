#include <stdio.h>

#include <iomanip>
#include <iostream>
#include <string>

#include "app/app.cpp"
#include "init.cpp"

int main(int argc, char* argv[]) {
  int size;
  int err = ParseArguments(argc, argv, size);
  if (err != 0) {
    return err;
  }

  Main(size);

  return 0;
}
