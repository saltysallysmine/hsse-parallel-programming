typedef struct node_s {
  struct node_s* next;
  int value;
} node_t;

typedef struct {
  node_t* head;
  node_t* tail;
} queue_t;

void queue_init(queue_t** queue);

void queue_push(queue_t* queue, int value);

int queue_pop(queue_t* queue, int* value);

void queue_destruct(queue_t* queue);

int queue_length(queue_t* queue);
