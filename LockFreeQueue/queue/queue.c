#include "queue.h"

#include <stdlib.h>

void queue_init(queue_t** queue) {
  *queue = (queue_t*)malloc(sizeof(queue_t));
  node_t* dummy = (node_t*)malloc(sizeof(node_t));
  dummy->value = -1;
  dummy->next = NULL;

  (*queue)->head = dummy;
  (*queue)->tail = dummy;
}

void queue_push(queue_t* queue, int value) {
  node_t* tail;
  node_t* next;

  node_t* node = (node_t*)malloc(sizeof(node_t));
  node->value = value;
  node->next = NULL;

  while (1) {
    tail = queue->tail;
    next = tail->next;

    if (tail == queue->tail) {
      if (next == NULL) {
        if (__sync_bool_compare_and_swap(&tail->next, next, node)) {
          break;
        }
      } else {
        __sync_bool_compare_and_swap(&queue->tail, tail, next);
      }
    }
  }
  __sync_bool_compare_and_swap(&queue->tail, tail, node);
}

int queue_pop(queue_t* queue, int* value) {
  node_t* head;
  node_t* tail;
  node_t* next;

  while (1) {
    head = queue->head;
    tail = queue->tail;
    next = head->next;

    if (head == queue->head) {
      if (head == tail) {
        if (next == NULL) {
          return 1;
        }
        __sync_bool_compare_and_swap(&queue->tail, tail, next);
      } else {
        *value = next->value;
        if (__sync_bool_compare_and_swap(&queue->head, head, next)) {
          break;
        }
      }
    }
  }
  free(head);

  return 0;
}

void queue_destruct(queue_t* queue) {
  int* value;
  while (queue_pop(queue, value) == 0) {
  }
  free(queue->head);
}

int queue_length(queue_t* queue) {
  size_t len = 0;
  node_t* current = queue->head;

  for (; current; ++len) {
    current = current->next;
  }
  return len;
}
