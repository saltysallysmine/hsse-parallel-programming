#include <mpi.h>
#include <stdio.h>

#include <iomanip>
#include <iostream>
#include <string>

#include "app/app.cpp"
#include "init.cpp"

int main(int argc, char* argv[]) {
  int max_x;
  int max_t;
  int err = ParseArguments(argc, argv, max_x, max_t);
  if (err != 0) {
    return err;
  }

  int rank, commsize;
  err = Init(argc, argv, rank, commsize);
  if (err != 0) {
    return err;
  }

  std::cout << "Hello from " << rank << std::endl;

  MPI_Barrier(MPI_COMM_WORLD);

  Main(rank, commsize, max_x, max_t);

  return MPI_Finalize();
}
