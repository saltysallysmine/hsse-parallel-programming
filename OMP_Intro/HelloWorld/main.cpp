#include <omp.h>

#include <iostream>
#include <vector>

int main() {
#pragma omp parallel
  { printf("Hellow from %d\n", omp_get_thread_num()); }

  return 0;
}
