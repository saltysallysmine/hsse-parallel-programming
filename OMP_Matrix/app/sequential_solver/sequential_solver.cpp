#include <iostream>
#include <vector>

#include "../model/matrix.h"

class SequentialSolver {
 public:
  void Multiply(Matrix& a, Matrix& b, Matrix& result);
};

void SequentialSolver::Multiply(Matrix& a, Matrix& b, Matrix& result) {
  int size = (int)a.size();
  for (int line_ind = 0; line_ind < size; ++line_ind) {
    for (int col_ind = 0; col_ind < size; ++col_ind) {
      for (int i = 0; i < size; ++i) {
        result[line_ind][col_ind] += a[line_ind][i] * b[i][col_ind];
      }
    }
  }
}
