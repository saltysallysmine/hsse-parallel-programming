#include <mpi.h>

#include <iostream>

int ParseArguments(int argc, char* argv[], int& send_type) {
  if (argc != 2) {
    std::cerr << "Exactly one number should be passed as an argument\n";
    return 1;
  }

  std::string input(argv[1]);

  try {
    send_type = std::stoi(input);
  } catch (...) {
    std::cerr << "Error: Invalid input for conversion" << std::endl;
    return 1;
  }
  return 0;
}

int Init(int argc, char* argv[], int& rank, int& commsize) {
  int err = MPI_Init(&argc, &argv);
  if (err != 0) {
    return 1;
  }

  err = MPI_Comm_size(MPI_COMM_WORLD, &commsize);
  if (err != 0) {
    return 1;
  }

  err = MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  if (err != 0) {
    return 1;
  }

  return 0;
}
