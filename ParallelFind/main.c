#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "finder/finder.h"
#include "utils/init.h"

int main(int argc, char* argv[]) {
  int size;
  int n_threads;
  if (Init(argc, argv, &size, &n_threads) != 0) {
    puts("Error occured. Terminate");
    return 1;
  }

  InitFinder(size);

  Generate();
  Print();
  Find(n_threads);

  DestructFinder();

  return 0;
}
