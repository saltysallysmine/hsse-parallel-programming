#include <mpi.h>
#include <stdio.h>

int main(int argc, char* argv[]) {
  int myrank, mycommsize;
  int err = MPI_Init(&argc, &argv);
  if (err != 0) {
    return 1;
  }

  err = MPI_Comm_size(MPI_COMM_WORLD, &mycommsize);
  if (err != 0) {
    return 1;
  }

  err = MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  if (err != 0) {
    return 1;
  }

  printf("Hello World. I am %d out of %d.\n", myrank, mycommsize);

  return MPI_Finalize();
}
