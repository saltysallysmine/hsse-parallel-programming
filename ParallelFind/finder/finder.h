typedef struct {
  int from;
  int to;
  int index;
} thread_args_t;

void InitFinder(int size);
void DestructFinder();

void Print();
void Generate();

void Find(int n_threads);
