#include <mpi.h>
#include <stdio.h>

#include <iomanip>
#include <iostream>
#include <string>

#include "utils/init.cpp"
#include "utils/spawner.cpp"

int main(int argc, char* argv[]) {
  int rank, commsize;
  int err = Init(argc, argv, rank, commsize);
  if (err != 0) {
    return err;
  }

  Main(rank, commsize);

  return MPI_Finalize();
}
