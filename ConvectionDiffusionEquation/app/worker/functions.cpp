#include <math.h>

long double F(long double x, long double t) { return x + t; }

// Явный левый уголок
long double CalcPoint(long double x, long double t, long double u_left,
                      long double u_center) {
  long double tao = 0.01;
  long double h = 0.01;

  x *= h;
  t *= tao;

  long double f = F(x, t);

  return tao * f - tao / h * (u_center - u_left) + u_center;
}
