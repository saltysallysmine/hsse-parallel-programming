#include "sorter.h"

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int _size;
int* _saved_array;
int* _array;

void InitSorter(int size) {
  _size = size;
  _array = (int*)malloc(sizeof(int) * _size);
  _saved_array = (int*)malloc(sizeof(int) * _size);
}

void DestructSorter() {
  free(_array);
  free(_saved_array);
}

// Utils

void Print() {
  for (int i = 0; i < _size; ++i) {
    printf("%d\t", _array[i]);
  }
  puts("");
}

void Generate() {
  srand(time(0));
  for (int i = 0; i < _size; ++i) {
    _array[i] = rand() % 100;
    _saved_array[i] = _array[i];
  }
}

void RollBack() {
  for (int i = 0; i < _size; ++i) {
    _array[i] = _saved_array[i];
  }
}

// Sort

static void Merge(int start, int mid, int end) {
  int left_ind = start;
  int right_ind = mid + 1;

  if (_array[mid] <= _array[right_ind]) {
    return;
  }

  while (left_ind <= mid && right_ind <= end) {
    if (_array[left_ind] <= _array[right_ind]) {
      ++left_ind;
      continue;
    }

    int temp = _array[right_ind];
    int cur_ind = right_ind;

    while (cur_ind != left_ind) {
      _array[cur_ind] = _array[cur_ind - 1];
      --cur_ind;
    }

    _array[left_ind] = temp;
    ++mid;
    ++left_ind;
    ++right_ind;
  }
}

static void* MergeFromThread(void* args) {
  int start = *(int*)args;
  int mid = *((int*)args + 1);
  int end = *((int*)args + 2);

  // printf("s %d m %d e %d\n", start, mid, end);
  Merge(start, mid, end);
  return NULL;
}

/* MergeSort
 * Merges two adjacent parts of the _array.
 *
 * Args:
 *  - Borders& b
 */
static void* MergeSort(void* args) {
  Borders b = {((int*)args)[0], ((int*)args)[1]};

  // printf("Hello from Sort from %d to %d\n", b.from, b.to);

  if (b.from >= b.to) {
    return NULL;
  }

  int mid = b.from + (b.to - b.from) / 2;

  Borders lb = {b.from, mid};
  Borders rb = {mid + 1, b.to};

  MergeSort(&lb);
  MergeSort(&rb);

  Merge(b.from, mid, b.to);

  return NULL;
}

void DummySort() {
  Borders b = {0, _size - 1};
  MergeSort(&b);
}

static void SortPartsWithThreads(int n_threads) {
  pthread_t threads[n_threads];
  Borders b_array[n_threads];

  for (int i = 0; i < n_threads; ++i) {
    b_array[i].from = _size / n_threads * i;
    b_array[i].to = _size / n_threads * (i + 1) - 1;

    if (i == n_threads - 1) {
      b_array[i].to = _size - 1;
    }

    // printf("Pthread from %d to %d\n", b.from, b.to);

    pthread_create(&threads[i], NULL, MergeSort, &b_array[i]);
  }

  for (int i = 0; i < n_threads; ++i) {
    pthread_join(threads[i], NULL);
  }
}

static void CalcBordersWithMid(BordersWithMid* b, int from, int f_part_size,
                               int s_part_size) {
  b->from = from;
  b->mid = b->from + f_part_size - 1;
  b->to = b->from + f_part_size + s_part_size - 1;
}

static void MergePartsWithThreads(int n_parts) {
  pthread_t threads[n_parts];
  BordersWithMid b_array[n_parts];

  int part_size = _size / n_parts;
  int last_part_size = _size - (n_parts - 1) * part_size;

  while (n_parts != 1) {
    for (int i = 0; i < n_parts - 1; i += 2) {
      if (i == n_parts - 2) {
        CalcBordersWithMid(&b_array[i], i * part_size, part_size,
                           last_part_size);
      } else {
        CalcBordersWithMid(&b_array[i], i * part_size, part_size, part_size);
      }

      pthread_create(&threads[i], NULL, MergeFromThread, &b_array[i]);
    }

    for (int i = 0; i < n_parts; i += 2) {
      pthread_join(threads[i], NULL);
    }

    if (n_parts % 2 == 0) {
      last_part_size = part_size + last_part_size;
    }

    part_size *= 2;
    n_parts = n_parts / 2 + n_parts % 2;

    // printf("New ps %d, np %d, lps %d\n", part_size, n_parts, last_part_size);
  }
}

void Sort(int n_threads) {
  SortPartsWithThreads(n_threads);
  MergePartsWithThreads(n_threads);
}

int CheckSort() {
  for (int i = 1; i < _size; ++i) {
    if (_array[i - 1] > _array[i]) {
      fprintf(stderr, "Not sorted! %d > %d", _array[i - 1], _array[i]);
      return 1;
    }
  }
  return 1;
}
