#include <time.h>

#include <fstream>
#include <iomanip>
#include <iostream>
#include <random>
#include <vector>

#include "model/matrix.h"
#include "parallel_solver/optimized_parallel_solver.cpp"
#include "parallel_solver/parallel_solver.cpp"
#include "sequential_solver/sequential_solver.cpp"

void PrintMatrix(Matrix& matrix) {
  for (Line line : matrix) {
    for (int el : line) {
      printf("%d\t", el);
    }
    puts("");
  }
}

void PrintMultiplies(Matrix& a, Matrix& b) {
  puts("A is");
  PrintMatrix(a);
  puts("");

  puts("B is");
  PrintMatrix(b);
  puts("");
}

void PrintResult(Matrix& res) {
  puts("Result is");
  PrintMatrix(res);
  puts("");
}

void PrintResultToFile(Matrix& res, const char* file_name) {
  std::ofstream file(file_name);

  for (Line line : res) {
    for (int el : line) {
      file << el << "\t";
    }
    file << "\n";
  }
}

Matrix GenerateMatrix(int& size) {
  Matrix result(size, Line(size));

  for (Line& line : result) {
    for (int& el : line) {
      el = rand() % 10;
    }
  }

  return result;
}

double get_elapsed_time_ms() {
  struct timespec ct;
  clock_gettime(CLOCK_MONOTONIC, &ct);
  return (double)ct.tv_sec * 1000.0 + (double)ct.tv_nsec / 1000000.0;
}

void MeasureSequentialMethod(int& size, Matrix& a, Matrix& b) {
  Matrix seq_res(size, Line(size, 0));

  double start_time = get_elapsed_time_ms();

  SequentialSolver seq_solver;
  seq_solver.Multiply(a, b, seq_res);

  double end_time = get_elapsed_time_ms();
  std::cout << "Sequential solver time: " << end_time - start_time << std::endl;

  PrintResultToFile(seq_res, "seq.txt");
}

void MeasureParallelMethod(int& size, Matrix& a, Matrix& b) {
  Matrix par_res(size, Line(size, 0));

  double start_time = get_elapsed_time_ms();

  ParallelSolver par_solver;
  par_solver.Multiply(a, b, par_res);

  double end_time = get_elapsed_time_ms();
  std::cout << "Parallel solver time: " << end_time - start_time << std::endl;

  PrintResultToFile(par_res, "par.txt");
}

void MeasureOptimizedParallelMethod(int& size, Matrix& a, Matrix& b) {
  Matrix par_res(size, Line(size, 0));

  double start_time = get_elapsed_time_ms();

  OptimizedParallelSolver opt_par_solver(size);
  opt_par_solver.Multiply(a, b, par_res);

  double end_time = get_elapsed_time_ms();
  std::cout << "Optimized parallel solver time: " << end_time - start_time
            << std::endl;

  PrintResultToFile(par_res, "opt_par.txt");
}

void Main(int& size) {
  srand(time(0));

  Matrix a = GenerateMatrix(size);
  Matrix b = GenerateMatrix(size);

  // PrintMultiplies(a, b);

  MeasureSequentialMethod(size, a, b);
  MeasureParallelMethod(size, a, b);
  MeasureOptimizedParallelMethod(size, a, b);
}
