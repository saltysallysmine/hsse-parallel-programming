#include <omp.h>

#include <iostream>
#include <vector>

int main() {
  int length;
  std::cin >> length;

  int n_threads = omp_get_max_threads();
  std::vector<long double> thread_value(n_threads + 1, 0);

#pragma omp parallel for
  for (int i = 1; i <= length; ++i) {
    thread_value[omp_get_thread_num()] += 1 / static_cast<long double>(i);
  }

  long double global_sum = 0.0;

  for (int i = 0; i < n_threads; ++i) {
    global_sum += thread_value[i];
  }

  std::cout << global_sum << std::endl;

  return 0;
}
