#include <mpi.h>
#include <stdio.h>

#include "utils/init.cpp"

int ProcessFirstPerformer(int commsize) {
  int message = 0;
  printf("Starting process from the first performer with value %d\n", message);

  int err = MPI_Send(&message, 1, MPI_INT, 1, 0, MPI_COMM_WORLD);
  if (err != 0) {
    return err;
  }

  MPI_Status status;
  err =
      MPI_Recv(&message, 1, MPI_INT, commsize - 1, 0, MPI_COMM_WORLD, &status);
  if (err != 0) {
    return err;
  }

  printf("Get value %d at the first performer\n", message);
  return 0;
}

int ProcessIntermediatePerformer(int rank, int commsize) {
  int message = 0;

  MPI_Status status;
  int err =
      MPI_Recv(&message, 1, MPI_INT, rank - 1, 0, MPI_COMM_WORLD, &status);
  if (err != 0) {
    return err;
  }

  ++message;
  printf("Get value at performer number %d. New value: %d\n", rank, message);

  err =
      MPI_Send(&message, 1, MPI_INT, (rank + 1) % commsize, 0, MPI_COMM_WORLD);

  return err;
}

int main(int argc, char* argv[]) {
  int rank, commsize;
  int err = Init(argc, argv, rank, commsize);
  if (err != 0) {
    return err;
  }

  if (rank == 0) {
    if (ProcessFirstPerformer(commsize) != 0) {
      return 1;
    }
  } else {
    if (ProcessIntermediatePerformer(rank, commsize) != 0) {
      return 1;
    }
  }

  return MPI_Finalize();
}
