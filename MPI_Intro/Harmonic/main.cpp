#include <mpi.h>

#include <iomanip>
#include <iostream>
#include <string>

void calculateSumPart(int start, int step, int series_length,
                      long double& answer) {
  for (int i = start; i <= series_length; i += step) {
    answer += 1. / static_cast<long double>(i);
  }
  // std::cout << "calculating " << answer << " in " << start - 1 << "\n";
}

int main(int argc, char* argv[]) {
  double start_time;
  double end_time;

  if (argc != 2) {
    std::cerr << "Exactly one number should be passed as an argument\n";
    return 1;
  }

  int series_length;
  std::string input(argv[1]);

  try {
    series_length = std::stoi(input);
  } catch (...) {
    std::cerr << "Error: Invalid input for conversion" << std::endl;
    return 1;
  }

  int rank, commsize;
  int err = MPI_Init(&argc, &argv);
  if (err != 0) {
    return 1;
  }

  err = MPI_Comm_size(MPI_COMM_WORLD, &commsize);
  if (err != 0) {
    return 1;
  }

  err = MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  if (err != 0) {
    return 1;
  }

  if (rank == 0) {
    start_time = MPI_Wtime();
  }

  long double local_result = 0;
  long double result = 0;
  calculateSumPart(rank + 1, commsize, series_length, local_result);

  MPI_Reduce(&local_result, &result, 1, MPI_LONG_DOUBLE, MPI_SUM, 0,
             MPI_COMM_WORLD);

  if (rank == 0) {
    std::cout << "Global result is " << std::setprecision(9) << result
              << std::endl;

    end_time = MPI_Wtime();
    std::cout << "Working time: " << std::setprecision(9)
              << end_time - start_time << std::endl;
  }

  return MPI_Finalize();
}
