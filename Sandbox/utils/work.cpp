#include <mpi.h>

#include <iostream>
#include <vector>

void DoSend() {
  MPI_Request req;
  std::vector<long double> data(10, 10.0);

  long double* data_ptr = &data[0];

  MPI_Isend(data_ptr, 5, MPI_LONG_DOUBLE, 1, 0, MPI_COMM_WORLD, &req);
}

void DoRecv() {
  long double* recv_data = new long double[5];
  MPI_Status stat;
  MPI_Recv(recv_data, 5, MPI_LONG_DOUBLE, 0, 0, MPI_COMM_WORLD, &stat);

  for (int i = 0; i < 5; ++i) {
    std::cout << recv_data[i] << " ";
  }
}

void DoMain(int rank, int commsize) {
  if (rank == 0) {
    DoSend();
  } else if (rank == 1) {
    DoRecv();
  } else {
    return;
  }
}
