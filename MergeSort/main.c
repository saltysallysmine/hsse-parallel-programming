#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "sorter/sorter.h"
#include "utils/init.h"

double get_elapsed_time_ms() {
  struct timespec ct;
  clock_gettime(CLOCK_MONOTONIC, &ct);
  return (double)ct.tv_sec * 1000.0 + (double)ct.tv_nsec / 1000000.0;
}

int main(int argc, char* argv[]) {
  int size;
  int n_threads;
  if (Init(argc, argv, &size, &n_threads) != 0) {
    puts("Error occured. Terminate");
    return 1;
  }

  InitSorter(size);

  Generate();
  double start = get_elapsed_time_ms();
  DummySort();
  printf("Dummy sort time: %f\n", get_elapsed_time_ms() - start);

  CheckSort();

  RollBack();
  start = get_elapsed_time_ms();
  Sort(n_threads);
  printf("Parallel sort time: %f\n", get_elapsed_time_ms() - start);

  CheckSort();

  DestructSorter();

  return 0;
}
