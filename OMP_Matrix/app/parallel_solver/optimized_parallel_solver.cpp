#include <omp.h>

#include <iostream>
#include <vector>

#include "../model/matrix.h"

class OptimizedParallelSolver {
 private:
  int size;
  Matrix _b_transposed;

 public:
  OptimizedParallelSolver(int& size);

  void TransposeB(Matrix& b);
  void Multiply(Matrix& a, Matrix& b, Matrix& result);
};

OptimizedParallelSolver::OptimizedParallelSolver(int& size)
    : size(size), _b_transposed(size, Line(size)) {
  for (Line& line : _b_transposed) {
    for (int& el : line) {
      el = 0;
    }
  }
}

void OptimizedParallelSolver::TransposeB(Matrix& b) {
#pragma omp parallel for
  for (int i = 0; i < size; ++i) {
#pragma omp parallel for
    for (int j = 0; j < size; ++j) {
      _b_transposed[j][i] = b[i][j];
    }
  }
}

void OptimizedParallelSolver::Multiply(Matrix& a, Matrix& b, Matrix& result) {
  TransposeB(b);

#pragma omp parallel for
  for (int line_ind = 0; line_ind < size; ++line_ind) {
#pragma omp parallel for
    for (int col_ind = 0; col_ind < size; ++col_ind) {
      for (int i = 0; i < size; ++i) {
        result[line_ind][col_ind] += a[line_ind][i] * _b_transposed[col_ind][i];
      }
    }
  }
}
