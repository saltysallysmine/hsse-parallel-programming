#include <iostream>

int ParseArguments(int argc, char* argv[], int& size) {
  if (argc != 2) {
    std::cerr << "Exactly one number should be passed in arguments\n";
    return 1;
  }

  std::string input_size(argv[1]);

  try {
    size = std::stoi(input_size);
  } catch (...) {
    std::cerr << "Error: Invalid size input for conversion" << std::endl;
    return 1;
  }

  return 0;
}
