#include <math.h>

long double Phi(long double x) {
  long double h = 0.01;
  return exp(-x * h);
}

long double Psi(long double t) {
  long double tao = 0.01;
  return cos(M_PI * t * tao);
}
