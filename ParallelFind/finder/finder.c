#include "finder.h"

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int _size;
int* _array;
int target;

void InitFinder(int size) {
  _size = size;
  _array = (int*)malloc(sizeof(int) * _size);
}

void DestructFinder() { free(_array); }

// Utils

void Print() {
  for (int i = 0; i < _size; ++i) {
    printf("%d\t", _array[i]);
  }
  puts("");
}

void Generate() {
  srand(time(0));
  for (int i = 0; i < _size; ++i) {
    _array[i] = rand() % 100;
  }

  target = _array[rand() % _size];
  printf("Will try to find %d\n", target);
}

// Find

static void* FindInPart(void* args) {
  thread_args_t* targs = (thread_args_t*)args;
  targs->index = -1;

  int t = target;

  for (int i = targs->from; i <= targs->to; ++i) {
    if (_array[i] == t) {
      targs->index = i;
    }
  }

  return NULL;
}

void Find(int n_threads) {
  pthread_t threads[n_threads];
  thread_args_t thread_args[n_threads];

  int part_size = _size / n_threads;
  int last_part_size = _size - (n_threads - 1) * part_size;

  for (int i = 0; i < n_threads; ++i) {
    thread_args[i].from = i * part_size;
    thread_args[i].to = i * part_size + part_size - 1;
    thread_args[i].index = -1;
    if (i == n_threads - 1) {
      thread_args[i].to = i * part_size + last_part_size - 1;
    }

    pthread_create(&threads[i], NULL, FindInPart, &thread_args[i]);
  }

  for (int i = 0; i < n_threads; ++i) {
    pthread_join(threads[i], NULL);
  }

  for (int i = 0; i < n_threads; ++i) {
    if (thread_args[i].index != -1) {
      printf("Id: %d\n", thread_args[i].index);
    }
  }
}
