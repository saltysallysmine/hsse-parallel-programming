#include <mpi.h>

#include <iomanip>
#include <iostream>

void Main(int rank, int commsize) {
  MPI_Comm spawn_comm;
  int n_spawn_proc = 4 * commsize;
  int err =
      MPI_Comm_spawn("./spawn", MPI_ARGV_NULL, n_spawn_proc, MPI_INFO_NULL, 0,
                     MPI_COMM_WORLD, &spawn_comm, MPI_ERRCODES_IGNORE);
  if (err != 0) {
    std::cerr << "Error while doing spawn in " << rank << " rank\n";
  } else {
    std::cout << "Spawned " << 4 << " new processes from " << rank << "\n";
  }
  std::cout << std::endl;
  std::cout.flush();
}
