#include <gmp.h>
#include <gmpxx.h>
#include <mpi.h>
#include <stdio.h>

#include <iomanip>
#include <iostream>
#include <string>

#include "utils/calculator.cpp"
#include "utils/init.cpp"

int main(int argc, char* argv[]) {
  int precision;
  int err = ParseArguments(argc, argv, precision);
  if (err != 0) {
    return err;
  }

  int rank, commsize;
  err = Init(argc, argv, rank, commsize);
  if (err != 0) {
    return err;
  }

  std::cout << "Hello from " << rank << "\n";

  MeasureExponentCalculation(rank, commsize, precision);

  return MPI_Finalize();
}
