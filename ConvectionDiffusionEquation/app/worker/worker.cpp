#include <mpi.h>

#include "functions.cpp"

class Worker {
  int rank;
  int max_x;
  int max_t;
  int chunk_size;

  long double left_x;
  long double right_x;

  long double* chunk;
  long double* next_chunk;
  long double neighbour_point;

  void _GetChunk();
  void _MakeNextChunk(long double t);

 public:
  Worker(int rank, int max_x, int max_t, int chunk_size)
      : rank(rank),
        max_x(max_x),
        max_t(max_t),
        chunk_size(chunk_size),
        left_x((rank - 1) * chunk_size),
        right_x(rank * chunk_size - 1),
        chunk(new long double[chunk_size]),
        next_chunk(new long double[chunk_size]) {}

  Worker(int rank, int max_x, int max_t, int chunk_size, int left_x,
         int right_x)
      : rank(rank),
        max_x(max_x),
        max_t(max_t),
        chunk_size(chunk_size),
        left_x(left_x),
        right_x(right_x),
        chunk(new long double[chunk_size]),
        next_chunk(new long double[chunk_size]) {}

  ~Worker() {
    delete[] chunk;
    delete[] next_chunk;
  }

  void Poll();
};

// Definitions

void Worker::_GetChunk() {
  MPI_Status status;
  MPI_Recv(chunk, chunk_size, MPI_LONG_DOUBLE, 0, 0, MPI_COMM_WORLD, &status);
}

void Worker::_MakeNextChunk(long double t) {
  next_chunk[0] = CalcPoint(left_x + 1, t - 1, neighbour_point, chunk[0]);
  for (int x = 1; x <= chunk_size - 1; ++x) {
    next_chunk[x] = CalcPoint(left_x + x + 1, t - 1, chunk[x - 1], chunk[x]);
  }
}

void Worker::Poll() {
  printf("Start worker %d from %Lf to %Lf\n", rank, left_x, right_x);
  puts("");

  _GetChunk();

  MPI_Status status;

  for (int t = 1; t < max_t; ++t) {
    MPI_Recv(&neighbour_point, 1, MPI_LONG_DOUBLE, 0, 0, MPI_COMM_WORLD,
             &status);

    _MakeNextChunk(t);

    MPI_Send(next_chunk, chunk_size, MPI_LONG_DOUBLE, 0, 0, MPI_COMM_WORLD);

    std::swap(chunk, next_chunk);
  }
}
