#include <mpi.h>

#include <iostream>

#include "utils/init.cpp"
#include "utils/send_recv.cpp"

int main(int argc, char* argv[]) {
  int send_type;
  int err = ParseArguments(argc, argv, send_type);
  if (err != 0) {
    return err;
  }

  int rank, commsize;
  err = Init(argc, argv, rank, commsize);
  if (err != 0 || commsize < 2) {
    return err;
  }

  DoSendTest(rank, commsize, send_type);

  return MPI_Finalize();
}
