typedef struct {
  int from;
  int to;
} Borders;

typedef struct {
  int from;
  int mid;
  int to;
} BordersWithMid;

void InitSorter(int size);
void DestructSorter();

void Print();
void Generate();
void RollBack();

void DummySort();
void Sort(int n_threads);

int CheckSort();
